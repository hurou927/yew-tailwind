use yew::prelude::*;

#[function_component(App)]
fn app() -> Html {
    html! {
        <>
            <h1>{ "Hello World" }</h1>
            <button class={classes!("inline-block","cursor-pointer","rounded-md","bg-gray-800","px-4","py-3","text-center","text-sm","font-semibold","uppercase","text-white","transition","duration-200","ease-in-out","hover:bg-gray-900")}>{"Button"}</button>
            <button class={classes!("btn")}>{"Button"}</button>
            <button class={"btn w-64 rounded-full"}>{"Button"}</button>
            <p class={classes!("bg-red-100")}>{"Test!"}</p>
            <button class={"btn btn-primary"}>{"One"}</button>
            <button class={"btn btn-secondary"}>{"Two"}</button>
            <button class={"btn btn-accent btn-outline"}>{"Three"}</button>
            <button class={"btn rounded-full"}>{"One"}</button>
            <button class={"btn rounded-none px-16"}>{"Two"}</button>
        </>
    }
}

fn main() {
    yew::start_app::<App>();
}
